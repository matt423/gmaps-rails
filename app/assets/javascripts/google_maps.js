(function($) {
    //Currently has a small dependency on Underscore.js, maybe consider two versions, with and without _.js
    $.google_maps = function(el, options) {

        var defaults = {
            propertyName: 'value',
            onSomeEvent: function() {}
        }

        var maps = this;        
        var current_latitude,current_longitde, center, map;
        var get_current = $.Deferred();
        

        maps.settings = {}

        var init = function() {
            maps.settings = $.extend({}, defaults, options);
            maps.el = el;
            maps.iconBase = '/assets/';
            maps.icons = {
              camp: {
                icon: maps.iconBase + 'mapmarker.png'
              }
            };
            maps.markers = []
            $(el).css({min_height: '500px'})            
        }

        maps.addMap = function(location) {
          findCenter(location);
          $.when(get_current).done(function() {     
            var mapOptions = {
              center : center,
              zoom: 14,
              mapTypeId: google.maps.MapTypeId.ROADMAP
            };         
            $.extend(maps.settings, mapOptions);
            maps.map = new google.maps.Map(mapElement(), mapOptions);
          });
        }

        maps.addMarker = function(location, title, infoWindow) {
          var marker_location;
          if(location == "center")
            marker_location = center        
          else
            marker_location = latLng(location.latitude, location.longitude)
          addMarkerToMap(marker_location, title);
          return this
        }
        
        //TODO accept a list of address components to return, i.e postal_town, locality, etc.. 
        //or use separate functions that return specified address component, i.e geoCodePostalTown                
        maps.geoCodeReverse = function(latitude, longitude, options) {  
          var geoCoder = new google.maps.Geocoder();
          var geoCodeDeferred = new jQuery.Deferred();
          var geoCodeLocation = latLng(latitude, longitude);
          geoCoder.geocode({'latLng': geoCodeLocation}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              maps.geoCodeAddressResult = results
              geoCodeDeferred.resolve(results);
            }
          });
          return geoCodeDeferred.promise();
        }
        
        maps.geoCodeAddressComponents = function() {
          return maps.geoCodeAddressResult[0].address_components       
        }
        
        maps.geoCodePostCode = function() {
          var postCode = filterGeoCodeResults(["postal_code"]);
          return _.isEmpty(postCode) ? "" : postCode[0].long_name;
        }

        maps.geoCodeEstablishment = function() {
          var establishment = filterGeoCodeResults(["establishment"]);
          return _.isEmpty(establishment) ? false : establishment;        
        }
        
        maps.geoCodeAdministrativeAreaLevel = function(options) {
          if(options.level) {
            var level = options.level;
            var administrativeAreaLevel = filterGeoCodeResults(["administrative_area_level_" + options.level])
            while(_.isEmpty(administrativeAreaLevel) && level <=3) {
              administrativeAreaLevel = filterGeoCodeResults(["administrative_area_level_" + level])
              if(options.level == 3) {
                level =1
                options.level = level
              }
              else
                level++;
              
            }
          }
          else
            administrativeAreaLevel = filterGeoCodeResults(["administrative_area_level_1"])[0].long_name;
          return administrativeAreaLevel[0].long_name
        }
        
        maps.geoCodeCountry = function() {
          return filterGeoCodeResults(["country"])[0].long_name
        }
        
        maps.geoCodeCountryCode = function() {
          return filterGeoCodeResults(["country"])[0].short_name
        }
                      
        maps.clearMarkers = function() {
          _.each(maps.markers, function(marker){
            marker.setMap(null);
          });
          maps.markers = [];
        }
        
        maps.locationClickEvent = function(callback) { 
          $.when(get_current).done(function() {
            google.maps.event.addListener(maps.map, 'click', function(event) {
              var latLng = event.latLng
              callback(latLng.lat(), latLng.lng())
            });   
          });             
        } 
        
        maps.resize = function() {
          google.maps.event.trigger(maps.map, 'resize');
          maps.map.setCenter(findCenter());          
        }
        //Private functions
        var latLng = function(latitude, longitude) {
          return new google.maps.LatLng(latitude, longitude);
        }
        
        var mapElement = function() {
          return $(maps.el)[0]
        }
        
        var addMarkerToMap = function(location, title) {
          var marker = new google.maps.Marker({
              position: location,
              map: maps.map,
              icon: maps.icons['camp'].icon,
              title: title
          });      
          maps.markers.push(marker)    
        }
        
        var findCenter = function(location) {
          if(location.center) {
            currentLocation = currentLocation();            
            $.when(get_current).done(function() {
              center = latLng(current_latitude, current_longitde);
              return center;
            });
          }
          else {          
            center = latLng(location.latitude, location.longitude);
            get_current.resolve();
            return center;
          }        
        }
        
        var currentLocation = function() {
          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(currentLocationCoords);
          }
          else
            console.error("wrong");          
        }
        
        var currentLocationCoords = function(position) {
          position = position.coords;
          current_longitde =  position.longitude;
          current_latitude = position.latitude;
          return get_current.resolve();
        }
        
        var filterGeoCodeResults = function(filter) {
          return _.filter(maps.geoCodeAddressComponents(), function(component) { return _.contains(filter, component.types[0]) });
        }
        
        var addMarkersToMap = function(markers) {
          //TODO Accept an array of marker locations
        }
        init();
    }

})(jQuery);
