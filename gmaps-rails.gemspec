# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'gmaps/rails/version'

Gem::Specification.new do |spec|
  spec.name          = "gmaps-rails"
  spec.version       = Gmaps::Rails::VERSION
  spec.authors       = ["matt"]
  spec.email         = ["matthew.a423@gmail.com"]
  spec.description   = %q{Simple gem that packages jQuery Google Maps Api}
  spec.summary       = %q{Rails gem that packages Google maps jquery library}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib", "app"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  
  spec.add_dependency "railties"
  spec.add_dependency "underscore-rails"
end
